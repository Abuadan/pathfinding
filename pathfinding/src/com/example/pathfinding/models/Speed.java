/**
 * 
 */
package com.example.pathfinding.models;



/**
 * @author imran
 * 
 */
public class Speed{
	
	public static final String TAG = Speed.class.getSimpleName();
	
	public static final int RIGHT = 1;
	public static final int LEFT = -1;
	public static final int UP = -1; 
	public static final int DOWN = 1;
	public static final float STOP = 0f;
	public static final float GO = 1.0f;
	public static final float FAST = 3.0f;
	
	
	private float xv, yv;
	private int dx,dy;
	

	public Speed(){
		this.xv = 0;
		this.yv = 0;
		this.dx = 0;
		this.dy = 0;
	}
	
	public Speed(float xv, float yv) {
		this.xv = xv;
		this.yv = yv;
	}
	
	
	public float getXv() {
		return xv;
	}

	public void setXv(float xv) {
		this.xv = xv;
	}

	public float getYv() {
		return yv;
	}

	public void setYv(float yv) {
		this.yv = yv;
	}

	public int getDx() {
		return dx;
	}


	public void setDx(int dx) {
		this.dx = dx;
	}


	public int getDy() {
		return dy;
	}


	public void setDy(int dy) {
		this.dy = dy;
	}

	public void moveUp(){
		this.dx  *= UP;
	}
	
	public void moveDown(){
		this.dx  *= DOWN;
	}
	
	public void moveLeft(){
		this.dx  *= LEFT;
	}
	
	public void moveRight(){
		this.dx  *= RIGHT;
	}
	
	@Override
	public String toString(){
		String str = "[speed_Object:" 
					+"xv= " + xv
					+ ",yv= " + yv
					+ ",dx= " + dx
					+ ",dy= " + dy
					+ "]";
		return str;
	}


	public void changeDx() {
		dx = dx * -1;
	}


	public void changeDy() {
		dy = dy * -1;
	}
}