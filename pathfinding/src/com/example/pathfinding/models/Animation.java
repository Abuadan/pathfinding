package com.example.pathfinding.models;

import java.util.LinkedList;
import java.util.List;

import android.graphics.Canvas;


/**
 * 
 * TODO Allow Path finding to work correctly.
 *
 */
public class Animation {
	
	private static final String TAG = Animation.class.getSimpleName();

	private Map map;
	private List<Target> targets = new LinkedList<Target>();;
	private List<Enemy> enemies = new LinkedList<Enemy>();
	
	public Animation(int mapWidth, int mapHeight) {
		initComp(mapWidth, mapHeight);
	}

	private void initComp(int w, int h) {
		map = new Map(w, h);
		configTargets();
		configEnemies();
	}
	
	private void configTargets() {
		int numTargets = 1;
		for (int i = 0; i < numTargets; i++)
			targets.add(Target.spawanAtRandomLocation());
	}

	private void configEnemies() {
		int numEnemies = 1;
		for (int i = 0; i < numEnemies; i++) {
			enemies.add(Enemy.spwanAtRandomLocation());
		}
		for(Enemy e: enemies){
			e.setTarget(getClosestTarget(e));
		}
	}
	
	
	private Target getClosestTarget(Enemy e) {
		if(targets.size() <= 0)
			return null;
		Target closest = targets.get(0);
		for(int i = 1; i<targets.size(); i++){
			if(distance(e.getCell(), targets.get(i).getCell()) < distance(e.getCell(), closest.getCell()))
				closest = targets.get(i);
			}
		
		return closest;
	}
	
	public void update() {
		for(Enemy e: enemies) e.update();
	}

	public void draw(Canvas canvas) {
		map.draw(canvas);	
		for (Enemy e : enemies) e.draw(canvas);
		for (Target t : targets) t.draw(canvas);
	}

	@Override
	public String toString() {
		return "Game [" 
				+ "player: " + "playername"
				+ "player_score:" + 0
				+ "number_of_active_towers:" + targets.size()
				+ "number_of_enemies: " + enemies.size()
				+"]";
	}
	
	public List<Target> getTargets() {
		return targets;
	}
	
	public String getTag() {
		return TAG;
	}

	public Map getGameMap() {
		return map;
	}

	public List<Enemy> getEnemies() {
		return enemies;
	}

	private static int distance(Cell src, Cell dst){
		return (int) Math.sqrt(Math.pow(Math.abs(dst.x - src.y), 2.0) + Math.pow(Math.abs(dst.x-src.y), 2.0));
	}
}
