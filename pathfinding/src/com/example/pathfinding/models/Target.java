package com.example.pathfinding.models;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.example.pathfinding.util.RandomGenerator;

public class Target {

	public static final String TAG = Target.class.getSimpleName();
	private static final Paint PAINT = new Paint();
	
	public int x,y;
	private Cell cell; 
	private Rect rect;
	
	public Target(int x, int y) {
		this.x = x;
		this.y = y;
		initComp();
	}

	
	private void initComp() {
		PAINT.setColor(Color.GREEN);
		PAINT.setStyle(Paint.Style.FILL);
		rect = new Rect(x, y, x + Renderable.RENDERABLE_WIDTH, y + Renderable.RENDERABLE_HEIGHT);
		cell = Map.getInstance().getMap2DGrid().getCellAtLocation(this.x, this.y);
		Map.getInstance().getMap2DGrid().getCellAtLocation(this.x, this.y).setOccupied(true);
	}

	public void update() {
		this.setCell(Map.getInstance().getMap2DGrid().getCellAtLocation(this.x, this.y));
		this.getCell().setOccupied(true);
	}
	
	
	public void draw(Canvas canvas) {
		canvas.drawRect(rect, PAINT);
	}
	
	@Override
	public String toString(){
		return "[Target_Object:" +"x= " + this.x + ",y= " + this.y + "]";
	
	}
	
	public static Target spawanAtRandomLocation() {
		int rowNum = RandomGenerator.pickRandomTRow(Map.getInstance().getMap2DGrid().getROWS());
		int colNum = RandomGenerator.pickRandomTCol(Map.getInstance().getMap2DGrid().getCOLS());
		int x = Map.getInstance().getMap2DGrid().getCELLS()[rowNum][colNum].getxPos();
		int y = Map.getInstance().getMap2DGrid().getCELLS()[rowNum][colNum].getyPos();
		return new Target(x, y);
	}

	public void setCell(Cell cell){
		this.cell = cell;
	}
	
	public Cell getCell(){
		return cell;
	}
}
