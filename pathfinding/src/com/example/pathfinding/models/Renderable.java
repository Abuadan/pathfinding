/**
 * 
 */
package com.example.pathfinding.models;

import android.graphics.Canvas;

/**
 * Renderable is an interface that must be implemented by any 
 * Game assest that is to be drawn on the screen. 
 * @author imran14
 *
 */
public interface Renderable {

	public static final String TAG = Renderable.class.getSimpleName();
	public static final int RENDERABLE_WIDTH = 50;
	public static final int RENDERABLE_HEIGHT = 50;
	
	public void draw(Canvas canvas);
}
