package com.example.pathfinding.models;

import java.util.List;

import android.graphics.Rect;


public final class Cell{
	
	private static final String TAG = Cell.class.getSimpleName();
	
	public final int x, y, endx, endy;
	
	private boolean occupied; // Indicates if the Cell is walk
	
	/**  USED FOR A* Search */
	private Cell parentCell;
	private int gCost;
	private int hCost;
	private int fCost;
		
	private final Rect cellRectangle;
	private List<Cell> allNeighbours;
	private List<Cell> fourNeighbours;
	

	
	public Cell(int x, int y, boolean isOccupied) {
		this.occupied = isOccupied;
		this.x = x;
		this.y = y;
		endx = x + Renderable.RENDERABLE_WIDTH;
		endy =y + Renderable.RENDERABLE_HEIGHT;
		cellRectangle = new Rect(x, y, endx, endx);
		gCost = 10;
		hCost = 0;
		fCost = 0;
	}


	public boolean isOccupied() {
		return occupied;
	}


	public void setOccupied(boolean isOccupied) {
		this.occupied = isOccupied;
	}

	public int getxPos() {
		return x;
	}


	public int getyPos() {
		return y;
	}

	
	public Rect getCellRectangle() {
		return cellRectangle;
	}

	
	public int getEndxPos() {
		return endx;
	}



	public int getEndyPos() {
		return endy;
	}
	

	public void setgCost(int gCost) {
		this.gCost = gCost;
	}
	

	public int getgCost() {
		return gCost;
	}
	

	public int gethCost() {
		return hCost;
	}


	public void sethCost(int hCost) {
		this.hCost = hCost;
	}


	public int getfCost() {
		return fCost;
	}


	public void setfCost(int fCost) {
		this.fCost = fCost;
	}


	public static String getTag() {
		return TAG;
	}
	

	public Cell getParentCell() {
		return parentCell;
	}


	public void setParentCell(Cell parentCell) {
		this.parentCell = parentCell;
	}
	

	public List<Cell> getAllNeighbours() {
		return allNeighbours;
	}


	public void setAllNeighbours(List<Cell> allNeighbours) {
		this.allNeighbours = allNeighbours;
	}

	public List<Cell> getFourNeighbours() {
		return fourNeighbours;
	}

	public void setFourNeighbours(List<Cell> fourNeighbours) {
		this.fourNeighbours = fourNeighbours;
	}

	
	@Override
	public String toString(){
		String str = "[Cell_Object:" 
					+"startX= " + x 
					+ ",startY= " + y
					+ ",endX= " + endx
					+ ",endY= " + endy
					+ ",Cell_width= " + Renderable.RENDERABLE_WIDTH
					+ ",Cell_height= " + Renderable.RENDERABLE_HEIGHT
					+ ",Cell_free= " + occupied
					+ "]";
		return str;
	}
}
