/**
 * 
 */
package com.example.pathfinding.models;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Imran
 * 
 */
public class PathFinder {

	public static final String TAG = PathFinder.class.getSimpleName();

	private static PathFinder pathFinder;
	private static final CellSorter CELL_SORTER = new CellSorter();


	public static PathFinder getPathFinder() {
		if (pathFinder == null)
			return new PathFinder();
		return pathFinder;
	}

	
	public static LinkedList<Cell> getShortestPath(Enemy e, Target t) {
		Cell start = e.getCell();
		Cell goal = t.getCell();

		start.setgCost(0);
		start.sethCost(getManhattanDistance(start, goal));
		start.setfCost(start.getgCost() + start.gethCost());

		List<Cell> openList = new LinkedList<Cell>();
		List<Cell> closedList = new LinkedList<Cell>();

		openList.add(start);
		while (openList.size() > 0) {
			Cell current = openList.get(0);
			for (Cell c : openList) {
				if (c.getfCost() < current.getfCost()) { // Find Cell with best F cost
					current = c;
				}
			}
			if (current.getxPos() == goal.getxPos()
					&& current.getyPos() == goal.getyPos()) {
				return generatePath(current);
			}

			openList.remove(current);
			closedList.add(current);

			for (Cell neighbour : current.getFourNeighbours()) {
				boolean better;
				if (closedList.contains(neighbour))
					continue;

				else {
					int neighbourDistanceFromStart = getManhattanDistance(neighbour, start);
					if (!openList.contains(neighbour)) {
						openList.add(neighbour);
						better = true;
					} else if (neighbourDistanceFromStart < getManhattanDistance(current, start)) {
						better = true;
					} else {
						better = false;
					}

					if (better) {
						neighbour.setParentCell(current);
						neighbour.setgCost(neighbourDistanceFromStart);
						neighbour.sethCost(getManhattanDistance(neighbour, goal));
						neighbour.setfCost(neighbour.getgCost()+ neighbour.gethCost());
					}
				}
			}
		}
		return null;
	}

	/**
	 * Reconstruct the path.
	 * 
	 * @param current
	 *            The last node to work backwards from to reconstruct the path.
	 */
	private static LinkedList<Cell> generatePath(Cell current) {
		LinkedList<Cell> p = new LinkedList<Cell>();
		while (current.getParentCell() != null) {
			p.add(current);
			current = current.getParentCell();
		}
		Collections.sort(p, CELL_SORTER);
		return p;
	}

	/**
	 * Return the Manhattan Distance between two Cells.
	 * @param a First Cell
	 * @param t Target Cell
	 * @return Distance.
	 */
	private static int getManhattanDistance(Cell a, Cell t) {
		return (Math.abs(a.getxPos() - t.getxPos()) + Math.abs(a.getyPos()- t.getyPos()));
	}

	
	private static class CellSorter implements Comparator<Cell> {
		public int compare(Cell lhs, Cell rhs) {
			return Math.abs(lhs.getxPos() - rhs.getxPos()) + Math.abs(lhs.getyPos() - rhs.getyPos());
		}
	}
}
