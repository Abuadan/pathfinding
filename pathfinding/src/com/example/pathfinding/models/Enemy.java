package com.example.pathfinding.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.example.pathfinding.util.RandomGenerator;

public class Enemy {

	public static final String TAG = Enemy.class.getSimpleName();
	
	private static final Paint PAINT = new Paint();
	
	private int x,y;
	private final Speed movment = new Speed(Speed.GO, Speed.GO);
	private final int RADIUS  = Renderable.RENDERABLE_WIDTH / 2;
	private Rect rect;
	private Cell cell;


	private Target closestTarget;
	private boolean targetFound;

	private LinkedList<Cell> path = new LinkedList<Cell>();
	private final ArrayList<Integer> path_x = new ArrayList<Integer>();
	private final ArrayList<Integer> path_y  = new ArrayList<Integer>();
	private boolean pathFound;

	
	public Enemy(int x, int y) {
		this.x = x;
		this.y = y;
		initEnemy();	
	}

	private void initEnemy() {
		PAINT.setColor(Color.RED);
		PAINT.setStyle(Paint.Style.FILL);
		targetFound = false;
		pathFound = false;
		rect = new Rect(x, y, x+Renderable.RENDERABLE_WIDTH, y + Renderable.RENDERABLE_HEIGHT);
		cell = Map.getInstance().getMap2DGrid().getCellAtLocation(x, y);
	}

	public void update() {
		searchAndDestroy(closestTarget);
	}

	private void searchAndDestroy(Target target) {
		if (!pathFound)
			requestPath(target);
		else
			travelPath();
	}
	
	private void requestPath(Target t) {
		path = PathFinder.getShortestPath(this, t);
		pathFound = true;
		Collections.reverse(path);
		createPath(Speed.FAST);
	}
	
	private void travelPath() {
		if (path_x.size()-1 > 0) {
			this.x = (path_x.remove(0));
			this.y = (path_y.remove(0));
		}
		rect.set(this.x, this.y, this.x + Renderable.RENDERABLE_WIDTH, this.y + Renderable.RENDERABLE_HEIGHT);
		cell = Map.getInstance().getMap2DGrid().getCellAtLocation(x, y);
	}
	
	private void createPath(float speed) {
		for(int i = 0; i < path.size()-1; i++){
			Cell current = path.get(i);
			Cell next = path.get(i+1);
			if (next.x > current.x) {
				for (int j = current.x; j < current.endx; j += speed) {
					path_x.add(j);
					path_y.add(current.y);
				}
			}
			if (next.y > current.y) {
				for (int j = current.y; j < current.endy; j += speed) {
					path_x.add(current.x);
					path_y.add(j);
				}
			}
			if (next.x < current.x) {
				for (int j = current.x; j >= next.x; j -= speed) {
					path_x.add(j);
					path_y.add(current.y);
				}
			}
			if (next.y < current.y) {
				for (int j = current.y; j >= next.y; j -= speed) {
					path_x.add(current.x);
					path_y.add(j);
				}
			}
		}
	}
	
	public void draw(Canvas canvas) {
		canvas.drawCircle(rect.exactCenterX(), rect.exactCenterY(), RADIUS, PAINT);
	}


	@Override
	public String toString() {
		String str = "[Enemy_Object:" + "x= " + this.x + ",y= " + this.y
				+ ",movment= " + movment.toString() + "]";
		return str;
	}

	public boolean hasFoundTarget(Target target) {
		return rect.intersect(target.getCell().getCellRectangle());
	}

	
	public static Enemy spwanAtRandomLocation() {
		int rowNum = RandomGenerator.pickRandomERow(Map.getInstance().getMap2DGrid().getROWS());
		int colNum = RandomGenerator.pickRandomECol(Map.getInstance().getMap2DGrid().getCOLS());
		int x = Map.getInstance().getMap2DGrid().getCELLS()[rowNum][colNum].getxPos();
		int y = Map.getInstance().getMap2DGrid().getCELLS()[rowNum][colNum].getyPos();
		return new Enemy(x, y);
	}

	public boolean isNearestTargetFound() {
		return targetFound;
	}

	public void setNearestTargetFound(boolean nearestTargetFound) {
		this.targetFound = nearestTargetFound;
	}

	public void setTarget(Target closestTarget) {
		this.closestTarget = closestTarget;
	}
	
	public Target getClosestTarget(){
		return this.closestTarget;
	}
	
	public void setCell(Cell cell){
		this.cell = cell;
	}
	
	public Cell getCell(){
		return cell;
	}
}