/**
 * 
 */
package com.example.pathfinding.models;

import android.graphics.Canvas;

/**
 * @author imran
 * 
 */
public class Map {

	public static final String TAG = Map.class.getSimpleName();
	private final Grid2DA map2DGrid;
	
	private static Map instance;


	public Map(int w, int h) {
		map2DGrid = new Grid2DA(w, h);
		map2DGrid.setUpAdjacencies();
		setInstance(this);
	}

	public void draw(Canvas canvas) {
		map2DGrid.draw2DGrid(canvas);
	}

	public Grid2DA getMap2DGrid() {
		return map2DGrid;
	}

	@Override
	public String toString() {
		String str = "[GameMap_Object:" + "Underlying_Grid= "
				+ map2DGrid.toString() + "]";
		return str;
	}

	public boolean isAtMapEdge(int x, int y) {
		return x <= 0 || x >= map2DGrid.getGridPixelWidth() - Renderable.RENDERABLE_WIDTH
				|| y <= 0 || y > map2DGrid.getGridPixelHeight()- Renderable.RENDERABLE_HEIGHT;
	}

	public boolean isAtRightEdge(int x) {
		return x >= map2DGrid.getGridPixelWidth() - Renderable.RENDERABLE_WIDTH;
	}

	public boolean isAtLeftEdge(int x) {
		return (x <= 0);
	}

	public boolean isAtTopEdge(int y) {
		return (y <= 0);
	}

	public boolean isAtBottomEdge(int y) {
		return y >= map2DGrid.getGridPixelHeight() - Renderable.RENDERABLE_HEIGHT;
	}

	public static Map getInstance() {
		return instance;
	}

	public static void setInstance(Map instance) {
		Map.instance = instance;
	}
}
