/**
 * 
 */
package com.example.pathfinding.activities;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.example.pathfinding.models.Animation;
import com.example.pathfinding.util.concurrent.MainThread;

public class DrawingPanel extends SurfaceView implements SurfaceHolder.Callback {

	public static final String TAG = DrawingPanel.class.getSimpleName();

	private static Animation game;
	private MainThread gameThread;

	public DrawingPanel(Context context) {
		super(context);
		getHolder().addCallback(this);
		gameThread = new MainThread(getHolder(), this);
		setFocusable(true);
	}

	public DrawingPanel(Context context, AttributeSet attrs) {
		super(context, attrs);
		getHolder().addCallback(this);
		gameThread = new MainThread(getHolder(), this);
		setFocusable(true);
	}

	public DrawingPanel(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		getHolder().addCallback(this);
		gameThread = new MainThread(getHolder(), this);
		setFocusable(true);
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,int height) { } /* Surface will not change */

	public void surfaceCreated(SurfaceHolder holder) {
		if (game == null)
			game = new Animation(this.getWidth(), this.getHeight());
		initiate();
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		stop();
	}

	public void update() {
		game.update();
	}

	public void render(Canvas canvas) {
		game.draw(canvas);
	}

	public void initiate() {
		if (gameThread.getState() == Thread.State.TERMINATED) { // "initiate" a new rendering thread
			p("Thread was terminated... creating new thread");
			if (game == null)
				game = new Animation(this.getWidth(), this.getHeight());
			gameThread = new MainThread(getHolder(), this);
			gameThread.setRunning(true);
			gameThread.start();
		} else {
			p("continuing thread");
			gameThread.setRunning(true);
			gameThread.start();
		}
	}

	public void stop() {
		gameThread.setRunning(false);
		boolean retry = true;
		while (retry) {
			try {
				gameThread.join();
				retry = false;
			} catch (InterruptedException e) {}
		}
	}

	public void p(String message) {
		Log.d(TAG, "Debug -- " + message);
	}
}
